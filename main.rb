Dir["./app/services/*.rb"].each {|file| require file }
Dir["./app/models/*.rb"].each {|file| require file }

class Main < Sinatra::Base
  puts "***************************************"
  puts "*****WELCOME TO QUAKE'S LOG REPORT*****"
  puts "***************************************"
  puts "***Just run the file: '2-games.txt' *** "

  puts "Enter file log game:"
  file_name = gets.chomp
  content = ReadFileService.get_file_content(file_name)
  game_information = InterpreterService.get_game_information(content)
  game_information.show_result

  puts "*********************************************"
  puts "IF YOU WANNA CHECK THE GAME STATS ON YOUR BROWSER,
                OPEN : LOCALHOST:9292"
  puts "****************************************************"
  Launchy.open("http://localhost:9292")

  get '/' do
    @matches = game_information.matches
    @ranking_players = game_information.ranking_of_players
    @ranking_deaths_reasons = game_information.ranking_of_deaths_reasons
    erb :index
  end
end
