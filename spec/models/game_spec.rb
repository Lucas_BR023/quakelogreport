require "spec_helper"

describe Game do
  before do
    @game = Game.new
    @match = Match.new
    @content = File.open('log_spec.txt', "r")
    @game = InterpreterService.get_game_information(@content)
  end
  it "game should have 2 matches" do
    expect(@game.matches.size).to eql(2)
  end

  it "ranking of players(kills) for all matches" do
    expect(@game.ranking_of_players).to eql([["Isgalamido", 28], ["Zeh", 22], ["Dono da Bola", 16], ["Assasinu Credi", 15], ["Mocinha", 0]])
  end

  it "ranking of deaths_reasons for all matches" do
    expect(@game.ranking_of_deaths_reasons).to eq([["MOD_ROCKET_SPLASH", 51], ["MOD_ROCKET", 21], ["MOD_FALLING", 12], ["MOD_TRIGGER_HURT", 11], ["MOD_RAILGUN", 8], ["MOD_MACHINEGUN", 4], ["MOD_SHOTGUN", 2]])
  end
end
