require "spec_helper"

describe Player do
  before do
    @player = Player.new
    @player.name = "Player Name"
    @player.killed_by = ["Player 1", "Player 2"]
    @player.kills = ["Player 1", "Player 2", "Player 3"]
    @player.suicides = 2
  end
  it 'player kills 3 enemies' do
    expect(@player.player_kills).to eql("#{@player.name} : #{@player.kills.size}")
  end
  it 'player_deaths should be 2' do
    expect(@player.player_deaths).to eql("#{@player.name} : #{@player.killed_by.size}")
  end
  it 'kills_death_ratio should be 1.5' do
    expect(@player.kills_death_ratio).to eql(1.5)
  end
  it 'player_score (kills - suicides) should be 1' do
    expect(@player.player_score).to eql(1)
  end
end
