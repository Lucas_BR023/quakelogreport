require "spec_helper"
require 'json'

describe Match do
  before do
    @game = Game.new
    @match = Match.new
    @content = File.open('log_spec.txt', "r")
    @game = InterpreterService.get_game_information(@content)
  end

  it "sum_kills method" do
    match = @game.matches.last
    expect(match.sum_kills).to eql(105)
  end

  it "players_order_by_kills" do
    match = @game.matches.last
    expect(match.players_order_by_kills.map {|player| player.name}).to eql(["Isgalamido", "Zeh", "Dono da Bola", "Assasinu Credi"])
  end

  it 'to_hash method' do
    expect(@game.matches.last.to_hash).to include(:start_game => '1:47',
                                                 :end_game => '12:13',
                                                 :total_kills => 105,
                                                 :players => ["Isgalamido", "Zeh", "Dono da Bola", "Assasinu Credi"],
                                                 :kills => ["Isgalamido : 27", "Zeh : 22", "Dono da Bola : 16", "Assasinu Credi : 15"])


  end

  it "to_s method: transform hash in a string" do
    match = @game.matches.last
    expect(match.to_s).to eql("#{match.name} : #{JSON.pretty_generate(match.to_hash)}")
  end

end
