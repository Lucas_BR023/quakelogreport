require "spec_helper"

describe ReadFileService do
   describe '.get_file_content' do
    it 'file exists' do
      expect(ReadFileService.get_file_content('test_file.txt').size).to be == 3
    end
    it 'file does not exists' do
      file_name = 'file_not_found'
      expect(STDOUT).to receive(:puts).with("file with name '#{file_name}' not found in this folder")
      ReadFileService.get_file_content(file_name)
    end
  end
end
