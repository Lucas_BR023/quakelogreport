require "spec_helper"

describe InterpreterService do
  before do
    @game = Game.new
    @match = Match.new
    @content = Array.new
  end
  describe "InitGame and ShutdownGame" do
    before do
      @content << '0:00 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\...'
      @content << '1:47 ShutdownGame:'
      @game = InterpreterService.get_game_information(@content)
    end
    it "get start_game" do
      expect(@game.matches.first.start_game.empty?).to eq(false)
    end
    it "get end_game" do
      expect(@game.matches.first.end_game.empty?).to eq(false)
    end
    it "create the object Match" do
      expect(@game.matches.size).to eq(1)
    end
  end

  describe "ClientUserinfoChanged" do
    before do
      @content << '0:00 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\...'
    end
    it "get User name" do
      @content << '1:47 ClientUserinfoChanged: 2 n\Dono da Bola\t\0\model\sarge\hmodel\sarge....'
      @content << '1:47 ShutdownGame:'
      @game = InterpreterService.get_game_information(@content)
      expect(@game.matches.first.players.size).to eq(1)
    end
    it "add same user" do
      @content << '1:47 ClientUserinfoChanged: 2 n\Dono da Bola\t\0\model\sarge\hmodel\sarge....'
      @content << '1:47 ClientUserinfoChanged: 2 n\Dono da Bola\t\0\model\sarge\hmodel\sarge....'
      @content << '1:47 ShutdownGame:'
      @game = InterpreterService.get_game_information(@content)
      expect(@game.matches.first.players.size).to eq(1)
    end
    it "Add a new player to match" do
      @content << '1:47 ClientUserinfoChanged: 2 n\Dono da Bola\t\0\model\sarge\hmodel\sarge....'
      @content << '1:47 ClientUserinfoChanged: 2 n\Isgalamido\t\0\model\sarge\hmodel\sarge....'
      @content << '1:47 ShutdownGame:'
      @game = InterpreterService.get_game_information(@content)
      expect(@game.matches.first.players.size).to eq(2)
    end
  end

  describe "killed" do
    before do
      @content << '0:00 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\...'
      @content << '1:47 ClientUserinfoChanged: 2 n\Oootsimo\t\0\model\sarge\hmodel\sarge....'
      @content << '1:47 ClientUserinfoChanged: 3 n\Zeh\t\0\model\uriel/zael\hmo...'
      @content << '0:25 Kill: 2 4 6: Oootsimo killed Zeh by MOD_ROCKET'
      @content << '23:06 Kill: 1022 2 22: <world> killed Zeh by MOD_TRIGGER_HURT'
      @content << '1:47 ShutdownGame:'
      @game = InterpreterService.get_game_information(@content)
    end
    it "get the user name who killed" do
      expect(@game.matches.first.players.find {|p| p.killed_by.size > 0}.name).to eql("Zeh")
    end
    it "get the user name who was killed" do
      expect(@game.matches.first.players.find {|p| p.kills.size > 0}.name).to eql("Oootsimo")
    end
    it "get death_reason" do
      expect(@game.matches.first.deaths_reasons.first ).to eq("MOD_ROCKET")
    end
    it "suicide when log line contain <world>" do
      expect(@game.matches.first.players.find {|p| p.suicides > 0}.name).to eq("Zeh")
    end
  end
end
