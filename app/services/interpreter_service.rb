class InterpreterService
 def self.get_game_information content
   game = Game.new
   match = Match.new
   content.each do |line|
     match = self.interpret_game_log(game, match, line)
   end
   game
 end

 def self.interpret_game_log game, match, line
   case line
     when /InitGame/
       match = self.init_game(game, match, line)
     when /ShutdownGame/
       match = self.end_game(game,match, line)
     when /ClientUserinfoChanged/
       match = self.connect_player(match,line)
     when /killed/
       match = self.kill(match,line)
     else
   end
  match
 end

 private
   def self.init_game(game,match,line)
     match = Match.new
     number_game = game.matches.size + 1
     match.name = "game_" + number_game.to_s
     match.start_game = line[/(\d)?\d:\d\d/]
     match
   end

   def self.end_game(game,match,line)
     game.matches << match
     match.end_game = line[/(\d)?\d:\d\d/]
     match
   end

   def self.connect_player(match,line)
     player = Player.new
     #regex to get name of user
     name = line[/n\\(.*?)\\t/m, 1]
     player.name = name
    found_player = self.get_player_name(match, name)
     unless found_player
       match.players << player
     end
     match
   end

   def self.kill(match,line)
     line["Kill:"] = ""
     who_killed = line.split(/: (.*?) killed/)[1]
     who_was_killed = line.split(/killed (.*?) by/)[1]
     death_reason = line.split(" ").last
     match.deaths_reasons << death_reason

     player_who_kill = self.get_player_name(match, who_killed)
     player_who_was_killed = self.get_player_name(match, who_was_killed)

     if who_killed.include?("<world>") || who_killed.eql?(who_was_killed)
       player_who_was_killed.suicides = player_who_was_killed.suicides + 1
     else
       player_who_kill.kills << who_was_killed
       player_who_was_killed.killed_by << who_killed
     end
     match
   end

   def self.get_player_name(match, name_param)
     match.players.select {|p| p.name == name_param}.first
   end
end
