class ReadFileService
  def self.get_file_content name
    begin
      file = File.open(name, 'r')
      content = file.readlines()
    rescue
      puts "file with name '#{name}' not found in this folder"
      exit
    end
    content
  end
end
