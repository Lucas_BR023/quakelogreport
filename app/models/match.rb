class Match
  attr_accessor :name, :players, :start_game, :end_game, :deaths_reasons
 def initialize
   @name
   @players = []
   @start_game
   @end_game
   @deaths_reasons = []
 end

 def sum_kills
  #use killed_by to show all kills including suicide kills
   sum_kills_match = 0
   self.players.each { |p| sum_kills_match += (p.killed_by.size + p.suicides) }
   sum_kills_match
  end

  def players_order_by_kills
    @players.sort_by!{|p| p.kills.size}.reverse!
  end

  def to_hash
    {
      :start_game => self.start_game,
      :end_game => self.end_game,
      :total_kills => self.sum_kills,
      :players => self.players_order_by_kills.map {|player| player.name},
      :kills => self.players_order_by_kills.map { |p| p.player_kills }
    }
  end

  def to_s
    "#{self.name} : #{JSON.pretty_generate(self.to_hash)}"
  end
end
