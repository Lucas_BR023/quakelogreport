class Player
 attr_accessor :name, :killed_by, :kills, :suicides
 def initialize
   @name
   @killed_by = []
   @kills = []
   @suicides = 0
 end
  def player_kills
    self.name + " : " + self.kills.size.to_s
  end

  def player_deaths
    self.name + " : " + self.killed_by.size.to_s
  end

  def player_kill_death_ratio
    self.name + " : " + self.kills_death_ratio.to_s
  end

  def player_score
    self.kills.size - self.suicides
  end

  def kills_death_ratio
    divider = self.killed_by.size > 0 ? self.killed_by.size : 1
    (self.kills.size.to_f / divider).round(2)
  end
end
