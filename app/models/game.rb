class Game
  attr_accessor :name, :matches
  def initialize
   @name
   @matches = []
  end

  def show_result
    self.matches.each do |m|
      puts m.to_s
    end
  end

  def ranking_of_players
   ranking_of_players = {}
   self.matches.each do |match|
     match.players.each do |player|
       if ranking_of_players[player.name].nil?
         ranking_of_players[player.name] = player.kills.size
       else
         ranking_of_players[player.name] = ranking_of_players[player.name] + player.kills.size
       end
     end
   end
   ranking_of_players.sort_by{|key,value|value}.reverse!
  end

  def ranking_of_deaths_reasons
   all_deaths_reasons = {}
   self.matches.each do |match|
     match.deaths_reasons.each do |death_reason|
       if all_deaths_reasons[death_reason].nil?
         all_deaths_reasons[death_reason] = 1
       else
         all_deaths_reasons[death_reason] = all_deaths_reasons[death_reason] + 1
       end
     end
   end
   all_deaths_reasons.sort_by{|key,value|value}.reverse!
  end
end
