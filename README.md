# Quake Log Report

Quake Log Report is an application that receives game logs, than extract valuable information (kills, deaths, players, etc), generating reports about the game matches, later presenting them in a user friendly way.

### Description

The application reads the game logs of Quake matches, keeping track of game metrics such as number of kills by player, deaths, suicides, final match score and gun used by the player:

* **Total kills**: Total of deaths and suicides of the match or player
* **Deaths**: when a player is killed by another player
* **Suicides**: When a player is killed by <world> (when he falls from a cliff, for example) 
```  
21:42 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT
```

or when he kills himself (while shooting with a rocket launch, for instance).
```  
22:40 Kill: 2 2 7: Isgalamido killed Isgalamido by MOD_ROCKET_SPLASH
```
* **Score**: Number of kills of the player minus the suicides
  

The output of the report is the following:
``` 
game_1: {
    total_kills: 45,
    players: ["Dono da bola", "Isgalamido", "Zeh"]
    kills: {
      "Dono da bola": 5,
      "Isgalamido": 18,
      "Zeh": 20
    }
  }
``` 

### Technology
Quake Log Report uses: 
* [Ruby](http://ruby-lang.org/pt/) - version 2.4
* [Sinatra](http://www.sinatrarb.com/) 
* [Twitter Bootstrap](http://getbootstrap.com)
* [RSpec](http://rspec.info)
 
### Installation
* Install Ruby(if you don't have it already);
* Download or clone sources from bitbucket.

#### Configuration
* On terminal, execute 'bundle install' in the application folder to add or update all gems;
* On the terminal, execute the **rackup** command in order to run the application.
```sh
$ bundle install
$ rackup
```
#### Testing
To run the tests of the project, simply run the following command, inside the project folder:

```sh
$ rspec
```

### Usage

The application receives a log file as parameter:
```
2-games.txt
```

This will generate the report on the console and open a web browser with html view. The user will be able to better visualize the report and also have ranking and other game statistics.
